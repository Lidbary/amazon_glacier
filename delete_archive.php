<?php

/**
 * Reference https://gist.github.com/Remiii/507f500b5c4e801e4ddc
 * How to delete an archive
 * The first steps will involve using the Amazon CLI (i.e https://aws.amazon.com/en/cli/)
 */

// Step 1 / Retrieve inventory
// $ aws glacier initiate-job --job-parameters '{"Type": "inventory-retrieval"}' --vault-name YOUR_VAULT_NAME --account-id YOUR_ACCOUNT_ID --region YOUR_REGION

// Wait during 3/5 hours… :-(

//For the new step you need to get the JobId. When the retrive inventory is done you can get it with the following command:
// aws glacier list-jobs --vault-name YOUR_VAULT_NAME --region YOUR_REGION

// Step 2 / Get the ArchivesIds
// $ aws glacier get-job-output --job-id YOUR_JOB_ID --vault-name YOUR_VAULT_NAME --region YOUR_REGION ./output.json
// See. Downloading a Vault Inventory in Amazon Glacier

// You can get all the ArchiveId in the ./output.json file.
// Step 3 / Delete Archives
$file = './output.json' ;
$accountId = 'YOUR_ACCOUNT_ID' ;
$region = 'YOUR_REGION' ;
$vaultName = 'YOUR_VAULT_NAME' ;

$string = file_get_contents ( $file ) ;
$json = json_decode($string, true ) ;
foreach ( $json [ 'ArchiveList' ] as $jsonArchives )
{
	echo 'Delete Archive: ' . $jsonArchives [ 'ArchiveId' ] . "\n" ;
	exec ( 'aws glacier delete-archive --archive-id="' . $jsonArchives [ 'ArchiveId' ] . '" --vault-name ' . $vaultName . ' --account-id ' . $accountId . ' --region ' . $region , $output ) ;
	echo $output ;
}

// NB: After you delete an archive, if you immediately download the vault inventory,
// it might include the deleted archive in the list because Amazon Glacier prepares vault inventory only about once a day.
// See. Deleting an Archive in Amazon Glacier