<?php

require 'vendor/autoload.php';

use Aws\Glacier\GlacierClient;
use Aws\Glacier\TreeHash;

$vaultName = "";
$glacier = new GlacierClient(array(
//	'profile' => 'default',
	'region' => 'us-east-2',
	'version' => '2012-06-01',
	'credentials' => array(
		'key'    => '',
		'secret' => '',
	)
));

$fileName = 'Files.zip'; // this is the file to upload
$chunkSize = 1024 * 1024 * pow(2,2); // 1 MB times a power of 2
$fileSize = filesize($fileName); // we will need the file size (in bytes)

// initiate the multipart upload
// it is dangerous to send the filename without escaping it first
$result = $glacier->initiateMultipartUpload(array(
	'archiveDescription' => 'A multipart-upload for file: '.$fileName,
	'partSize' => $chunkSize,
	'vaultName' => $vaultName
));

// we need the upload ID when uploading the parts
$uploadId = $result['uploadId'];

// we need to generate the SHA256 tree hash
// open the file so we can get a hash from its contents
$fp = fopen($fileName, 'r');
// This class can generate the hash
$th = new TreeHash();
// feed in all of the data
$th->update(fread($fp, $fileSize));
// generate the hash (this comes out as binary data)...
$hash = $th->complete();
// but the API needs hex (thanks). PHP to the rescue!
$hash = bin2hex($hash);

// reset the file position indicator
fseek($fp, 0);

// the part counter
$partNumber = 0;

print("Uploading: '".$fileName
      ."' (".$fileSize." bytes) in "
      .(ceil($fileSize/$chunkSize))." parts...\n");
while ($partNumber * $chunkSize < ($fileSize + 1))
{
// while we haven't written everything out yet
// figure out the offset for the first and last byte of this chunk
	$firstByte = $partNumber * $chunkSize;
// the last byte for this piece is either the last byte in this chunk, or
// the end of the file, whichever is less
// (watch for those Obi-Wan errors)
	$lastByte = min((($partNumber + 1) * $chunkSize) - 1, $fileSize - 1);

// upload the next piece
	$result = $glacier->uploadMultipartPart(array(
		'body' => fread($fp, $chunkSize), // read the next chunk
		'uploadId' => $uploadId, // the multipart upload this is for
		'vaultName' => $vaultName,
		'range' => 'bytes '.$firstByte.'-'.$lastByte.'/*' // weird string
	));

// this is where one would check the results for error.
// This is left as an exercise for the reader ;)

// onto the next piece
	$partNumber++;
	print("\tpart ".$partNumber." uploaded...\n");
}
print("...done\n");

// and now we can close off this upload
$result = $glacier->completeMultipartUpload(array(
	'archiveSize' => $fileSize, // the total file size
	'uploadId' => $uploadId, // the upload id
	'vaultName' => $vaultName,
	'checksum' => $hash // here is where we need the tree hash
));

// this is where one would check the results for error.
// This is left as an exercise for the reader ;)

// get the archive id.
// You will need this to refer to this upload in the future.
$archiveId = $result->get('archiveId');

print("The archive Id is: ".$archiveId."\n");

?>