<?php

// Include the SDK using the Composer autoloader
require 'vendor/autoload.php';

use Aws\Glacier\GlacierClient;

$client    = GlacierClient::factory( array(
	'region'      => 'us-east-2',
	'version'     => '2012-06-01',
	'credentials' => array(
		'key'    => '',
		'secret' => '',
	)
) );
$vaultName = "";
$filename  = "README.md";
$result    = $client->uploadArchive( array(
	'vaultName' => $vaultName,
	'body'      => fopen( $filename, 'r' ),
) );
//$archiveId = $result->get( 'archiveId' );
echo "<pre>".var_export($result,TRUE)."</pre>";
